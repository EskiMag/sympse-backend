require 'sinatra'
require 'logger'
require 'json'

class App < Sinatra::Base

  configure do
    set :raise_errors, true
    set :public_folder, 'public'
  end


  def initialize
    super
    @db = {}
  end


  def delete_older_than_1_minute
    @db.delete_if { |k, v| (Time.now - v[:time]).to_i > 60 }
  end


  def setup_headers_for_cors
    headers('Access-Control-Allow-Origin' => '*')
    headers('Access-Control-Allow-Methods' => 'GET,POST,OPTIONS')
    headers("Access-Control-Allow-Headers" => "X-Requested-With")
  end


  get '/location/:token' do
  	@token = params[:token]
    @location = @db[@token]
    if @location
      erb :index
    else
      status 404
      erb :notfound
    end
  end


  get '/lastLocation/:token' do
    token = params[:token]
    location = @db[token]
    location.to_json
  end


  options '/all' do
    setup_headers_for_cors

    @db.to_json
  end


  get '/all' do
    delete_older_than_1_minute
    setup_headers_for_cors
    
    @db.to_json
  end


  get '/generateToken' do
    Digest::MD5.hexdigest(rand(100000000).to_s)
  end


  get '/saveLocation' do
    token = params.delete('token')
  	location = params
    location[:time] = Time.now.to_i
  	@db[token] = location

    "OK"
  end

end